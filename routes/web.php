<?php

use App\Models\Note;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NoteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('notes', [NoteController::class, 'create'])->name('notes.create');
Route::post('notes', [NoteController::class, 'store'])->name('notes.store');
Route::get('notes/{note}', [NoteController::class, 'view'])->name('notes.view');
