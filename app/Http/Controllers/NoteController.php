<?php

namespace App\Http\Controllers;

use App\Models\Note;
use Illuminate\Http\Request;

class NoteController extends Controller
{
    public function create()
    {
        return view('notes/create');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'remaining_views' => 'numeric',
            'content' => 'required|string',
        ]);

        $note = Note::create($data);

        return to_route('notes.view', $note);
    }

    public function view(Note $note)
    {


        $note->remaining_views -= 1 ;

        $note->save();
        return view('notes/view', ['note' => $note]);
    }



}
